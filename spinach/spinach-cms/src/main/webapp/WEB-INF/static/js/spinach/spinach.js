/**
 * 自定义jquery函数
 */
(function($) {
	// 从json对象中获取值填入form表单中
	$.fn.jsonData = function(data) {
		var $name = $(this).find("[name]");
		$name.each(function () {
			var name = $(this).attr("name");
			if ($(this).is("input")) {
				$(this).val(data[name]);
			} else if ($(this).is("select")) {
				$(this).find("option").each(function () {
					if ($(this).val() == data[name]) {
						$(this).prop("selected", true);
					}
				});
			} else if ($(this).is("img")) {
				$(this).prop("src", data[name]);
			} else {
				$(this).html(data[name]);
			}
		});
	};
	// 表单验证提交 配合validation.js使用
	$.fn.validFrom = function(callback) {
		$(this).Validform({
			tiptype : 2,
			ajaxPost : true,
			postonce : true,
			callback : function(data) {
				$.Hidemsg();
				callback(data);
			}
		});
	}

	/**
	 * 表单验证,弹出提示
	 * @param beforeSubmit
	 * @param callback
	 */
	$.fn.validForm_tip = function (beforeSubmit, callback) {
		var $valid_div = $(this).find(".valid_div");
		$valid_div.each(function () {
			var $info = $(this).find(".info");
			$info.html($valid_checktip)
		});
		return $(this).Validform({
			beforeSubmit: beforeSubmit(),
			ajaxPost: true,
			postonce: true,
			callback: function (data) {
				$.Hidemsg();
				callback(data);
			},
			tiptype: function (msg, o, cssctl) {
				//msg：提示信息;
				//o:{obj:*,type:*,curform:*}, obj指向的是当前验证的表单元素（或表单对象），type指示提示的状态，值为1、2、3、4， 1：正在检测/提交数据，2：通过验证，3：验证失败，4：提示ignore状态, curform为当前form对象;
				//cssctl:内置的提示信息样式控制函数，该函数需传入两个参数：显示提示信息的对象 和 当前提示的状态（既形参o中的type）;
				if (!o.obj.is("form")) {//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
					var $valid_div = o.obj.parents(".valid_div");
					var infoObj = $valid_div.find(".info");
					var $valid_input = $valid_div.find(".valid_input");
					var objtip = $valid_div.find(".Validform_checktip");
					cssctl(objtip, o.type);
					objtip.text(msg);
					if (o.type == 2) {
						infoObj.fadeOut(200);
					} else {
						if (infoObj.is(":visible")) {
							return;
						}
						var left = $valid_input.offset().left,
							top = $valid_input.offset().top;
						if ($valid_input.is("input[type='radio']") || $valid_input.is("input[type='checkbox']")) {
							left = left;
						} else {
							var wid = $valid_input.width();
							left = left + wid - 30;
						}
						infoObj.css({
							'position': 'fixed',
							left: left,
							top: top - 45
						}).show().animate({
							top: top - 35
						}, 200);
					}

				}
			}
		})
	}
})(jQuery);

/** 全屏弹出 */
function full_add(title, url) {
	var index = layer.open({
		type : 2,
		title : title,
		content : url
	});
	layer.full(index);
}

//时间格式化
function dataFormate(fmt,date) { //author: meizz
	var d = Number(date);
	if(d){
		d=new Date(date);
	}else{
		return '';
	}
	var o = {
		"M+": d.getMonth() + 1, //月份
		"d+": d.getDate(), //日
		"h+": d.getHours(), //小时
		"m+": d.getMinutes(), //分
		"s+": d.getSeconds(), //秒
		"q+": Math.floor((d.getMonth() + 3) / 3), //季度
		"S": d.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (d.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}
/**
 * 返回按钮
 */
$(document).on("click",".btn-default",function(){
	layer_close();
})

/* 弹出窗口 */
function window_add(title, url, w, h) {
	layer_show(title, url, w, h);
}
/**
 *
 * @param formId 查询条件表单id
 * @param tmplId 模板id
 * @param showDivId 数据需要显示到的元素的id
 * @param pageDivId 分页dom id
 */
function initPage(formId, tmplId, showDivId, pageDivId) {
	var $form = $("#" + formId);
	var $showDiv = $("#" + showDivId);
	var $pageDiv = $("#" + pageDivId);
	var url = $form.attr("action");
	var tmpl = $("#"+tmplId).html();
	var $showtdiv = $showDiv.parents("table").next('div .showmsg');
	var $showtcontent = '<div class="showmsg" style="color:red;width:100%;text-align:center;"></div>';
	if(null==$showtdiv||$showtdiv.length==0){
		$showDiv.parents("table").after($showtcontent);
	}
	$showDiv.parents("table").next('div .showmsg').html('');
	$.ajax({
		dataType: "json",
		url: url,
		type:"post",
		data:$form.serialize(),
		success: function(data){
			var totalPage = data.pagination.totalPage;
			$showDiv.parents("table").next('div .showmsg').html('');
			if(data.pagination.total==0&&data.pagination.total==0){
				$showDiv.parents("table").next('div .showmsg').html('没有查询到数据!');
			}
			laytpl(tmpl).render(data, function(html) {
				$showDiv.html(html);
			});
			data=null;
			if(totalPage>=0){//大于1页才显示分页
				laypage({
					cont : $pageDiv, // 容器。值支持id名、原生dom对象，jquery对象。
					pages : totalPage, // 通过后台拿到的总页数
					curr:  1,
					jump : function(e,first) { // 触发分页后的回调
						$form.find("input[name='page']").val(e.curr);
						if(!first){
							$.ajax({
								dataType: "json",
								url: url,
								type:"post",
								data:$form.serialize(),
								success: function(res){
									e.pages = e.last = res.pagination.totalPage; // 重新获取总页数，一般不用写
									laytpl(tmpl).render(res, function(html) {
										$showDiv.html(html);
									});
								}
							});
						}
					}
				});
			}else{
				$pageDiv.html('');
			}
			if(totalPage==1){
				$pageDiv.find(".laypage_curr").hide();
			} else {
				$pageDiv.find(".laypage_curr").show();

			}
		}
	});
}